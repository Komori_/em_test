#include <cassert>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <unordered_map>
#include <vector>
typedef long double calc_float_t;

// 定数 k
constexpr int kMixtureNum = 5;
// 定数 n
constexpr int kChiMax = 200;
// 定数 N
constexpr int kDataNum = 1000000;
// 初期値を変えて実行する回数
constexpr int kExecNum = 5;

struct BinomialMixture;

// Combination
constexpr calc_float_t kUnknownMemo = - 1;
calc_float_t g_memo[kChiMax + 1][kChiMax + 1];
calc_float_t combination(int n, int k);
// p_n(x, mu)
calc_float_t binomial_prob(calc_float_t mu, int x, int n);
// 分布 bm から，観測値データを生成する．
std::vector<int> generate_data(const BinomialMixture& bm);
// bm1 と bm2 のパラメータベクトルの差のノルムを計算する
calc_float_t diff_norm(const BinomialMixture& bm1, const BinomialMixture& bm2);
// EM アルゴリズムの本体
BinomialMixture em_iteration(const std::vector<int>& data, const BinomialMixture& theta_old);

struct BinomialMixture {
  std::vector<calc_float_t> pi;
  std::vector<calc_float_t> mu;

  // mu は[0.0, 1.0]の一様乱数，pi は mt() を正規化して用いる
  BinomialMixture(void) : pi(kMixtureNum), mu(kMixtureNum) {}

  // 適当に初期値を pi と mu に与えるを関数
  void rand_generate(void) {
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<calc_float_t> uniform_01(0.0, 1.0);
    calc_float_t pi_sum = 0.0;
    for (int i = 0; i < kMixtureNum; ++i) {
      pi[i]   = static_cast<calc_float_t>(mt());
      pi_sum += pi[i];
      mu[i]   = uniform_01(mt);
    }

    // pi は和が 1.0 という制約があるので，適当に正規化する
    assert(pi_sum > 0.0);
    for (int i = 0; i < kMixtureNum; ++i) {
      pi[i] /= pi_sum;
    }

    sort();
  }

  // 分布は外からは見えないので，元の分布の順番通りに推定されるわけではない
  // よって，mu の値の降順に並び替えたほうが分かりやすいことが多い
  void sort(void) {
    // insertion sort で mu が昇順になるようにソートする
    for (int i = 1; i < kMixtureNum; ++i) {
      if (mu[i - 1] > mu[i]) {  // 並び替える必要があるかどか判定
        int j = i;
        calc_float_t pi_tmp = pi[i];
        calc_float_t mu_tmp = mu[i];
        // mu_tmp を挿入すべき箇所まで中身を 1 つ次にコピーしていく
        do {
          pi[j] = pi[j - 1];
          mu[j] = mu[j - 1];
        } while ((--j) > 0 && mu[j - 1] > mu_tmp);
        // 現在指している先が挿入すべき index
        pi[j] = pi_tmp;
        mu[j] = mu_tmp;
      }
    }
  }

  // 対数尤度
  calc_float_t log_likelyhood(const std::vector<int>& data) const {
    calc_float_t ret = 0.0;
    size_t data_size = data.size();

#pragma omp parallel for reduction(+:ret)
    for (size_t t = 0; t < data_size; ++t) {
      calc_float_t prob = 0.0;
      for (int j = 0; j < kMixtureNum; ++j) {
        prob += pi[j] * binomial_prob(mu[j], data[t], kChiMax);
      }
      ret += std::log(prob);
    }

    return ret;
  }
};

BinomialMixture g_ans;
// BinomialMixture を ostream に流すための関数
std::ostream& operator <<(std::ostream& os, const BinomialMixture& bm) {
  os << "pi: ";
  for (int i = 0; i < kMixtureNum; ++i) {
    os << bm.pi[i] << " ";
  }
  os << std::endl;

  os << "mu: ";
  for (int i = 0; i < kMixtureNum; ++i) {
    os << bm.mu[i] << " ";
  }
  os << std::endl;

  for (int i = 0; i < kMixtureNum; ++i) {
    int rank = 0;
    for (int j = 0; j < kMixtureNum; ++j) {
      if (bm.pi[i] > bm.pi[j]) ++rank;
    }
    os << rank << " ";
  }

  return os;
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    printf("usage: ./main.out OUTPUT_FILENAME");
    return EXIT_FAILURE;
  }
  std::string filename(argv[1]);

  // combination の memo の初期化
  for (int i = 0; i < kChiMax + 1; ++i) {
    for (int j = 0; j < kChiMax + 1; ++j) {
      g_memo[i][j] = kUnknownMemo;
    }
  }

  for (int i = 0; i < kChiMax + 1; ++i) {
    for (int j = 0; j <= i; ++j) {
      combination(i, j);
    }
  }

  // 生成に用いる分布
  BinomialMixture bm;
  bm.rand_generate();
  g_ans = bm;
  std::cout << "[bm]" << std::endl;
  std::cout << bm << std::endl;
  // 推定に用いるデータ
  std::vector<int> data = generate_data(bm);
  for (int j = 0; j < kExecNum; ++j) {
    std::ofstream ofs((filename + std::to_string(j) + ".txt").c_str());
    // printf("[%d]\n", j);
    // 分布のパラメータの暫定推定値(決め方をあとで吟味する)
    BinomialMixture theta;
    theta.rand_generate();
    printf("[%d]\n", j);
    std::cout << theta << std::endl;

    calc_float_t prev_likelyhood = -1.0E+8;
    for (int i = 0; ; ++i) {
      // iteration を回す
      theta = em_iteration(data, theta);
      theta.sort();

      calc_float_t likelyhood = theta.log_likelyhood(data);
      // 標準出力に収束状況を出力する
      ofs << i + 1 << " " << likelyhood << std::endl;

      // 差が十分に小さくなったらループを終了する
      if (std::abs(prev_likelyhood - likelyhood) < 1E+0) {
        break;
      }
      prev_likelyhood = likelyhood;
    }
  }

  return EXIT_SUCCESS;
}

// 数が大きくなりやすいので，calc_float_t で考える
inline calc_float_t combination(int n, int k) {
  calc_float_t& memo = g_memo[n][k];
  if (memo != kUnknownMemo) return memo;
  if (k == n || k == 0)     return (memo = 1);
  return (memo = combination(n - 1, k - 1) + combination(n - 1, k));
}

// X = 0 .. n の二項分布の確率関数
inline calc_float_t binomial_prob(calc_float_t mu, int x, int n) {
  calc_float_t ret = std::pow(mu, x) * std::pow(1.0 - mu, n - x);
  return combination(n, x) * ret;
}

std::vector<int> generate_data(const BinomialMixture& bm) {
  std::vector<int> ret(kDataNum);
  std::random_device rd;
  std::mt19937       mt(rd());
  // 0 から 1 の一様乱数を振って，出た値に応じて pi を選択する
  std::uniform_real_distribution<calc_float_t> uniform_01(0.0, 1.0);

#pragma omp parallel for
  for (int i = 0; i < kDataNum; ++i) {
    calc_float_t pi_sum  = 0.0;
    calc_float_t pi_rand = uniform_01(mt);
    // 万が一 for 文を回して決まらなかったら，最後の分布から生成するようする
    int selected_idx = kMixtureNum - 1;
    for (int j = 0; j < kMixtureNum; ++j) {
      pi_sum += bm.pi[j];
      if (pi_rand < pi_sum) {
        selected_idx = j;
        break;
      }
    }

    std::binomial_distribution<int> bd(kChiMax, bm.mu[selected_idx]);
    ret[i] = bd(mt);
  }

  return ret;
}

calc_float_t diff_norm(const BinomialMixture& bm1, const BinomialMixture& bm2) {
  calc_float_t sum = 0.0;
  for (int i = 0; i < kMixtureNum; ++i) {
    calc_float_t pi_diff = bm1.pi[i] - bm2.pi[i];
    calc_float_t mu_diff = bm1.mu[i] - bm2.mu[i];
    sum += pi_diff * pi_diff;
    sum += mu_diff * mu_diff;
  }

  return std::sqrt(sum);
}

BinomialMixture em_iteration(const std::vector<int>& data,
    const BinomialMixture& theta_old) {
  BinomialMixture theta_new;
  int data_size = data.size();
  // 尤度の分母(そこそこ重い計算なので先に算出しておく)
  std::vector<calc_float_t> denominators(kDataNum);
#pragma omp parallel for
  for (int t = 0; t < data_size; ++t) {
    denominators[t] = 0.0;
    for (int l = 0; l < kMixtureNum; ++l) {
      denominators[t] += theta_old.pi[l]
        * binomial_prob(theta_old.mu[l], data[t], kChiMax);
    }
    denominators[t] = 1.0 / denominators[t];
  }

#pragma omp parallel for
  for (int i = 0; i < kMixtureNum; ++i) {
    calc_float_t pi_i = 0.0;
    calc_float_t mu_numerator   = 0.0;
    calc_float_t mu_denominator = 0.0;
    for (int t = 0; t < data_size; ++t) {
      calc_float_t likelyhood = theta_old.pi[i]
        * binomial_prob(theta_old.mu[i], data[t], kChiMax) * denominators[t];
      if (!isnan(likelyhood)) {
        pi_i += likelyhood;
        mu_denominator += likelyhood;
        mu_numerator   += likelyhood * data[t];
      }
    }

    theta_new.pi[i] = pi_i * (1.0 / kDataNum);
    theta_new.mu[i] = mu_numerator / (mu_denominator * kChiMax);
  }

  return theta_new;
}
